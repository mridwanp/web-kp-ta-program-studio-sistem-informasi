<div class="main-sidebar sidebar-style-2">
    <aside id="sidebar-wrapper">
        <div class="sidebar-brand">
            <h4></h4>
            <a href="index.html"><img width="120" src="/assets/img/logo-v2.png"></a>
        </div>
        <div class="sidebar-brand sidebar-brand-sm">
            <a href="index.html"><img src="/assets/img/logo.png" width="70"></a>
        </div>
        <ul class="sidebar-menu">
            <li class="menu-header">Progress Tugas Akhir</li>
            <li><a class="nav-link" href=<?php echo url('dashboard-mahasiswa') ?>></i>
                    <span>Progress Tugas Akhir</span></a></li>
            <li class="menu-header">Management Pendaftaran Tugas Akhir</li>
            <li><a class="nav-link" href=<?php echo url('dashboard-mahasiswa-proposal-ta') ?>><i
                        class="fas fa-sticky-note"></i> <span>Proposal Tugas
                        Akhir</span></a></li>
            <li><a class="nav-link" href=<?php echo url('dashboard-mahasiswa-seminar-ta') ?>><i
                        class="fas fa-book-open"></i>
                    <span>Seminar Tugas
                        Akhir</span></a></li>
            <li><a class="nav-link" href=<?php echo url('dashboard-mahasiswa-sidang-ta') ?>><i
                        class="fas fa-book-reader"></i>
                    <span>Sidang Tugas
                        Akhir</span></a></li>
            <li><a class="nav-link" href=<?php echo url('dashboard-mahasiswa-yudisium') ?>><i
                        class="fas fa-sticky-note"></i>
                    <span>Yudisium</span></a></li>
            <li class="menu-header">Management Pendaftaran Kerja Praktik</li>
            <li><a class="nav-link" href=<?php echo url('dashboard-mahasiswa-form-001') ?>><i
                class="fas fa-sticky-note"></i> <span>Pengajuan Kerja Praktik (Form-001)</span></a></li>
            <li><a class="nav-link" href=<?php echo url('dashboard-mahasiswa-kp') ?>><i
                        class="fas fa-sticky-note"></i> <span>Daftar Kerja Praktik</span></a></li> 
            <li><a class="nav-link" href=<?php echo url('dashboard-mahasiswa-sidang-kp') ?>><i
                        class="fas fa-book-open"></i> <span>Sidang Kerja Praktik</span></a></li>
            <!--<li class="menu-header">Pengguna</li>
<li><a class="nav-link" href="#"><i class="fas fa-users"></i> <span>Pengguna Admin</span></a></li>
<li><a class="nav-link" href="#"><i class="fas fa-users"></i> <span>Pengguna Mahasiswa</span></a></li>

<li class="menu-header">Management Tugas Akhir</li>
<li><a class="nav-link" href=""><i class="fas fa-file"></i> <span>Data Tugas Akhir</span></a></li>-->
        </ul>
    </aside>
</div>
